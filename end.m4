divert[]dnl
#!/bin/sh

undivert(_AF_DECL)

##
## Define usage()
##
usage()
{
        cat <<EOF
Usage: []_AF_NAME[]-config [[OPTIONS]] [[LIBRARIES]]
Options:
undivert(_AF_USAGE_OPTIONS)dnl
Libraries:
undivert(_AF_USAGE_LIBRARIES)dnl
EOF
        exit $1
}

##
## Process options
##
parse()
{
# we must be called with at least one argument
if test $# -eq 0; then
        usage 1 1>&2
fi

# at least one option should be selected
case "$1" in
  --*)
     ;;
  *)
     usage 1 1>&2
     ;;
esac

# grab all -- arguments
while test $# -gt 0; do
  case "$1" in
  -*=*) af_optarg=`echo "$1" | sed 's/[[-_a-zA-Z0-9]]*=//'` ;;
  *) af_optarg= ;;
  esac

  case $1 in
undivert(_AF_PROC_OPTIONS)dnl
    --*)
      usage 1 1>&2
      ;;
    *)
      break
      ;;
  esac
  [shift]
done

# if we have a default library use it
if test $# -eq 0; then
if test "X$af_lib_default" != "X"; then
  af_lib_[]_AF_SAFE(_AF_LIB_DEFAULT)=yes
  return
fi
fi

while test $# -gt 0; do
  case $1 in
undivert(_AF_PROC_LIBRARIES)dnl
    *)
      usage 1 1>&2
      ;;
  esac
  [shift]
done
}

print_result()
{
if test "X$af_echo_cflags" = "Xyes"; then
    af_all_flags="$af_cflags"
fi

if test "X$af_echo_libs_L" = "Xyes" || test "X$af_echo_libs_l" = "Xyes"; then
    af_all_flags="$af_all_flags $af_libs"
fi

if test -z "$af_all_flags" || test "X$af_all_flags" = "X "; then
    exit 1
fi

# Straight out any possible duplicates, but be careful to
# get `-lfoo -lbar -lbaz' for `-lfoo -lbaz -lbar -lbaz'
af_other_flags=
af_lib_L_flags=
af_rev_libs=
for i in $af_all_flags; do
    case "$i" in
    # a library, save it for later, in reverse order
    -l*) af_rev_libs="$i $af_rev_libs" ;;
    -L*|-R*)
        if test "X$af_echo_libs_L" = "Xyes"; then
            case " $af_lib_L_flags " in
            *\ $i\ *) ;;                              # already there
            *) af_lib_L_flags="$af_lib_L_flags $i" ;; # add it to output
            esac 
        fi;;
    *)
        case " $af_other_flags " in
        *\ $i\ *) ;;                                  # already there
        *) af_other_flags="$af_other_flags $i" ;;     # add it to output
        esac ;;
    esac
done

af_ord_libs=
if test "X$af_echo_libs_l" = "Xyes"; then
    for i in $af_rev_libs; do
        case " $af_ord_libs " in
        *\ $i\ *) ;;                          # already there
        *) af_ord_libs="$i $af_ord_libs" ;;   # add it to output in reverse order
        esac
    done
fi

echo $af_other_flags $af_lib_L_flags $af_ord_libs
}

##
## Main Body
##

parse $*

undivert(_AF_PRE_BODY)
undivert(_AF_BODY)
undivert(_AF_POST_BODY)

print_result

exit 0


divert(-1)dnl
##
##  M4 Quotas are hard to work with, so use braces like autoconf
##    (which are matched by vi, emacs)
changequote(, )
changequote([, ])

##
##  This m4 script will use a set of diversion to collect data
##    for different sections of the script
##
define([_AF_DECL],2)      # before parse or usage defined
define([_AF_PRE_BODY],3)  # before main body
define([_AF_BODY],1)      # where most text goes
define([_AF_POST_BODY],4) # after rest of text

define([_AF_PROC_OPTIONS],5)
define([_AF_PROC_LIBRARIES],6)

define([_AF_USAGE_OPTIONS],8)
define([_AF_USAGE_LIBRARIES],9)

define([DIV_PUSH],[pushdef([AF_DIVERSION],divnum)divert($1)dnl])
define([DIV_POP],[divert(AF_DIVERSION)popdef([AF_DIVERSION])dnl])

define([_AF_SAFE],[translit([$*],[-],[_])])

##
## AF_INIT(config_name)
##   Inits the engine (name is required)
define([AF_INIT],[define([_AF_NAME],[$1])])


##################################################################
### ARGUMENT OPTIONS
##################################################################

##
## AF_ARG_DIR(directory_name,[default_value])
##   Adds setable directory option.
##
##  Example:
##    AF_ARG_DIR(@prefix@)
##     gives --prefix[=DIR] option with default of @prefix@ from autoconf
define([AF_ARG_DIR],[dnl
divert(_AF_USAGE_OPTIONS)dnl
  --[$1][[=DIR]] 
divert(_AF_PROC_OPTIONS)dnl
    [--]$1[]=*)
      [$1]=$af_optarg
      [af_]$1[_set]=yes
      ;;
    [--]$1)
      [af_echo_]$1[]=yes
      ;;
divert(_AF_DECL)dnl
ifelse([$2],[],,[$1=$2])
divert(_AF_BODY)dnl
if test "X$af_echo_[$1]" = "Xyes"; then
        echo $[$1]
fi
])


##
## AF_COPY_DIR(dir1,dir2)
##   Copies the directory setting of dir1 to the dir2,
##   if one is set but not other
define([AF_COPY_DIR],[dnl
divert(_AF_PRE_BODY)dnl
if test "X[$af_]$1[_set]" = "Xyes" && test "X[$af_]$2[_set]" = "X"; then
[$2]=$[$1]
fi
divert(_AF_BODY)dnl
])


##
## AF_ARG_VERSION(version_string)
##   --version will give defined string
define([AF_ARG_VERSION],[dnl
divert(_AF_USAGE_OPTIONS)dnl
  --version     output _AF_NAME version information
divert(_AF_PROC_OPTIONS)dnl
    --version)
      af_echo_version=yes
      ;;
divert(_AF_BODY)dnl
if test "X$af_echo_version" = "Xyes"; then
        echo [$*]
        exit 0
fi
])

##
##  AF_ARG_MSG(message)
##    Just adds a message to the options list
define([AF_ARG_MSG],[dnl
divert(_AF_USAGE_OPTIONS)dnl
$*
divert(_AF_BODY)dnl
])

##
##  AF_ARG_SET_OPTION(name,usage_message,action)
##    adds a nonstandard setable (name=blah) option with message and action.
##    additional arguments are in $af_optarg
define([AF_ARG_SET_OPTION],[dnl
divert(_AF_USAGE_OPTIONS)dnl
$2
divert(_AF_PROC_OPTIONS)dnl
    [--]$1=*)
$3
      ;;
divert(_AF_BODY)dnl
])

##
##  AF_ARG(name,usage_message,action)
##    adds a nonstandard option with message and action.
##    additional arguments are in $af_optarg
define([AF_ARG],[dnl
divert(_AF_USAGE_OPTIONS)dnl
$2
divert(_AF_PROC_OPTIONS)dnl
    [--]$1*)
$3
      ;;
divert(_AF_BODY)dnl
])

##################################################################
### LIBRARIES
##################################################################


##
## AF_LIBRARY(libname,set_action)
##   define a library option
define([AF_LIBRARY],[dnl
divert(_AF_USAGE_LIBRARIES)dnl
         [$1]
divert(_AF_PROC_LIBRARIES)dnl
    [$1])
      af_lib_[]_AF_SAFE($1)=yes
      ;;
divert(_AF_BODY)dnl
ifelse([$2],[],,[dnl
if test "X$af_lib_[]_AF_SAFE($1)" = "Xyes"; then
$2
fi
])dnl
])


##
##  AF_LIB_DEFAULT(libname)
##    mark a library as the default if none are given
define([AF_LIB_DEFAULT],[dnl
divert(_AF_DECL)dnl
af_lib_default=_AF_SAFE($1)
define([_AF_LIB_DEFAULT],[$1])dnl
divert(_AF_BODY)dnl
])


##
## AF_DEF_LIBS(libraries)
##  defines libaries to be added
define([AF_DEF_LIBS],[af_libs="$* $af_libs"])


##
## AF_DEF_CFLAGS(cflags)
##  defines cflags to be added
define([AF_DEF_CFLAGS],[af_cflags="$af_cflags $*"])

##
## AF_DECL(sh_statement)
##  Places a sh statement before main body.
define([AF_DECL],[dnl
divert(_AF_DECL)dnl
$*
divert(_AF_BODY)dnl
])

##################################################################
### MISC 
##################################################################

##
## Standard options
##
divert(_AF_USAGE_OPTIONS)dnl
  --cflags      print pre-processor and compiler flags
  --libs        print library linking information
  --libs-dirs   only print the -L/-R part of --libs
  --libs-names  only print the -l part of --libs
  --help        display this help and exit
divert(_AF_PROC_OPTIONS)dnl
    --help)
      usage 0 0>&2
      ;;
    --cflags)
      af_echo_cflags=yes
      ;;
    --libs)
      af_echo_libs_L=yes
      af_echo_libs_l=yes
      ;;
    --libs-dirs)
      af_echo_libs_L=yes
      ;;
    --libs-names)
      af_echo_libs_l=yes
      ;;
divert(_AF_DECL)dnl
af_libs=
af_cflags=
divert(-1)


divert(_AF_BODY)dnl
